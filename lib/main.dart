import 'package:flutter/material.dart';

void main() => runApp(MyApp());


class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context)  {
    return MaterialApp(
      title: 'Welcome to Flutter',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Welcome to Flutter'),
        ),
        body: Center(
          child: Text ('''  Hi, my name is Edgar Ramirez.
          I will be graduating in Fall 2020. 
          The world is merciless, but it is also very beautiful. '''),

        ),
      ),
    );
  }
}
